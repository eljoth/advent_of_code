package main

import (
	"flag"
	"fmt"

	"gitlab.com/eljoth/advent_of_code/internal/runner"
)

func main() {
	year := flag.Int("year", 19, "year to run")
	day := flag.Int("day", 1, "day to run")
	part := flag.Int("part", 1, "part to run")
	flag.Parse()

	solver := runner.Get(*year, *day, *part)
	result := solver()
	fmt.Print(result)
}
