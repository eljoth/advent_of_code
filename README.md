# advent_of_code

## Requirements

You need to have Go and [Task](taskfile.dev) installed.

## Setup

To create a new year, run:

```bash
task -- 22_twentytwo
```

It will generate asset files under `assets/22` and code files under `internal/22_twentytwo`, including the runner.

A sample code file looks like:

```go
package twentytwo

// oneA solves the first part of the puzzle. It needs to take the input of type that oneP returns.
func oneA(input any) string {
	//TODO implement me
	panic("implement me")
}


// oneB is the solution for part B. It needs to take the same input as oneA.
func oneB(input any) string {
	//TODO implement me
	panic("implement me")
}

// oneP is the input processor, it takes a string and can give back any type you like
func oneP(s string) any {
	return ""
}
```

After defining the exercise specific datatype, remember to also replace the type in the runner:

```go
// day.Day[any] should be updated to day.Day[theTypeYouDefined]
1:  day.Day[any]{A: oneA, B: oneB, P: oneP}.ReadFile(y, 1).ProcessInput(),
```

After implementing your solution, you can run it with:

```bash
go run cmd/aoc/main.go -year=22 -day=1 -part=1
```
