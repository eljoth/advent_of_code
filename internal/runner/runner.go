package runner

import (
	"fmt"

	blank "gitlab.com/eljoth/advent_of_code/internal/00_blank"
	"gitlab.com/eljoth/advent_of_code/internal/pkg/year"
)

type Y interface {
	Day(int) year.D
}

func Get(y, d, p int) func() string {
	var sY Y
	switch y {
	case 00:
		sY = blank.Get()
	default:
		return func() string {
			return fmt.Sprintf("No solution for %d/%d/%d", y, d, p)
		}
	}
	sD := sY.Day(d)

	if p == 1 {
		return sD.GetA
	}
	return sD.GetB
}
