package day

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/eljoth/advent_of_code/assets"
)

type solverFunc[T any] func(T) string

type processorFunc[T any] func(string) T

type Day[T any] struct {
	Input          string
	ProcessedInput T
	A              solverFunc[T]
	B              solverFunc[T]
	P              processorFunc[T]
}

func (d Day[T]) GetA() string {
	return d.A(d.ProcessedInput)
}

func (d Day[T]) GetB() string {
	return d.B(d.ProcessedInput)
}

func (d Day[T]) ReadFile(year, day int) Day[T] {
	name := fmt.Sprintf("%02d/%02d.txt", year, day)
	fileContent, err := assets.Assets.ReadFile(name)
	if err != nil {
		log.Printf("Error reading file: %s\n", name)
	}
	d.Input = strings.TrimSuffix(string(fileContent), "\n")
	return d
}

func (d Day[T]) ProcessInput() Day[T] {
	processedInput := d.P(d.Input)
	d.ProcessedInput = processedInput
	return d
}
