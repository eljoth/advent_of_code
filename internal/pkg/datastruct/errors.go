package datastruct

var (
	ErrEmptyStack = newError("stack is empty")
)

type dsError struct {
	msg string
}

func (d dsError) Error() string {
	return d.msg
}

func newError(msg string) error {
	return dsError{msg}
}
