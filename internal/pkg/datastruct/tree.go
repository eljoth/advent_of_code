package datastruct

type Node[T any] struct {
	Value    T
	Parent   *Node[T]
	Children map[string]*Node[T]
}

type Tree[T any] struct {
	root *Node[T]
}

func NewTree[T any](v T) *Tree[T] {
	return &Tree[T]{
		root: &Node[T]{
			Value:    v,
			Children: map[string]*Node[T]{},
		},
	}
}

func (t *Tree[T]) Root() *Node[T] {
	return t.root
}

func (n *Node[T]) Add(id string, v T) {
	newNode := &Node[T]{
		Value:    v,
		Parent:   n,
		Children: map[string]*Node[T]{},
	}
	n.Children[id] = newNode
}

func (n *Node[T]) Get(id string) *Node[T] {
	if node, ok := n.Children[id]; ok {
		return node
	}
	return nil
}
