package test

import "testing"

func Wrapper[T any](solver func(T) string, processor func(string) T) func(string) string {
	return func(input string) string {
		processed := processor(input)
		return solver(processed)
	}
}

func Bencher[T any](solver func(T) string, processor func(string) T) func(*testing.B, string) {
	return func(b *testing.B, input string) {
		for i := 0; i < b.N; i++ {
			processed := processor(input)
			solver(processed)
		}
	}
}
