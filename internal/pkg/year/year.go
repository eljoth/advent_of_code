package year

type D interface {
	GetA() string
	GetB() string
}

type Year struct {
	Days map[int]D
}

func (y Year) Day(day int) D {
	return y.Days[day]
}
