package blank

import (
	"gitlab.com/eljoth/advent_of_code/internal/pkg/day"
	"gitlab.com/eljoth/advent_of_code/internal/pkg/year"
)

func Get() Year {

	y := 0

	return Year{
		Year: &year.Year{
			Days: map[int]year.D{
				1:  day.Day[any]{A: oneA, B: oneB, P: oneP}.ReadFile(y, 1).ProcessInput(),
				2:  day.Day[any]{A: twoA, B: twoB, P: twoP}.ReadFile(y, 2).ProcessInput(),
				3:  day.Day[any]{A: threeA, B: threeB, P: threeP}.ReadFile(y, 3).ProcessInput(),
				4:  day.Day[any]{A: fourA, B: fourB, P: fourP}.ReadFile(y, 4).ProcessInput(),
				5:  day.Day[any]{A: fiveA, B: fiveB, P: fiveP}.ReadFile(y, 5).ProcessInput(),
				6:  day.Day[any]{A: sixA, B: sixB, P: sixP}.ReadFile(y, 6).ProcessInput(),
				7:  day.Day[any]{A: sevenA, B: sevenB, P: sevenP}.ReadFile(y, 7).ProcessInput(),
				8:  day.Day[any]{A: eightA, B: eightB, P: eightP}.ReadFile(y, 8).ProcessInput(),
				9:  day.Day[any]{A: nineA, B: nineB, P: nineP}.ReadFile(y, 9).ProcessInput(),
				10: day.Day[any]{A: tenA, B: TenB, P: TenP}.ReadFile(y, 10).ProcessInput(),
				11: day.Day[any]{A: elevenA, B: elevenB, P: elevenP}.ReadFile(y, 11).ProcessInput(),
				12: day.Day[any]{A: twelveA, B: twelveB, P: twelveP}.ReadFile(y, 12).ProcessInput(),
				13: day.Day[any]{A: thirteenA, B: thirteenB, P: thirteenP}.ReadFile(y, 13).ProcessInput(),
				14: day.Day[any]{A: fourteenA, B: fourteenB, P: fourteenP}.ReadFile(y, 14).ProcessInput(),
				15: day.Day[any]{A: fifteenA, B: fifteenB, P: fifteenP}.ReadFile(y, 15).ProcessInput(),
				16: day.Day[any]{A: sixteenA, B: sixteenB, P: sixteenP}.ReadFile(y, 16).ProcessInput(),
				17: day.Day[any]{A: seventeenA, B: seventeenB, P: seventeenP}.ReadFile(y, 17).ProcessInput(),
				18: day.Day[any]{A: eighteenA, B: eighteenB, P: eighteenP}.ReadFile(y, 18).ProcessInput(),
				19: day.Day[any]{A: nineteenA, B: nineteenB, P: nineteenP}.ReadFile(y, 19).ProcessInput(),
				20: day.Day[any]{A: twentyA, B: twentyB, P: twentyP}.ReadFile(y, 20).ProcessInput(),
				21: day.Day[any]{A: twentyOneA, B: twentyOneB, P: twentyOneP}.ReadFile(y, 21).ProcessInput(),
				22: day.Day[any]{A: twentyTwoA, B: twentyTwoB, P: twentyTwoP}.ReadFile(y, 22).ProcessInput(),
				23: day.Day[any]{A: twentyThreeA, B: twentyThreeB, P: twentyThreeP}.ReadFile(y, 23).ProcessInput(),
				24: day.Day[any]{A: twentyFourA, B: twentyFourB, P: twentyFourP}.ReadFile(y, 24).ProcessInput(),
				25: day.Day[any]{A: twentyFiveA, B: twentyFiveB, P: twentyFiveP}.ReadFile(y, 25).ProcessInput(),
			},
		},
	}
}

type Year struct {
	*year.Year
}
