package blank

import (
	"testing"

	"gitlab.com/eljoth/advent_of_code/internal/pkg/day"
	"gitlab.com/eljoth/advent_of_code/internal/pkg/test"
)

func TestRunner(t *testing.T) {
	tests := []struct {
		name  string
		solve func(string) string
		input string
		want  string
	}{
		{"0", test.Wrapper[any](oneA, oneP), "1797", "1797"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := tt.solve(tt.input)
			if got != tt.want {
				t.Errorf("got %s, want %s", got, tt.want)
			}
		})
	}
}

func BenchmarkRunner(b *testing.B) {
	tests := []struct {
		name  string
		bench func(*testing.B, string)
		input string
	}{
		{"1a", test.Bencher[any](oneA, oneP), day.Day[string]{}.ReadFile(15, 1).Input},
	}

	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			tt.bench(b, tt.input)
		})
	}
}
